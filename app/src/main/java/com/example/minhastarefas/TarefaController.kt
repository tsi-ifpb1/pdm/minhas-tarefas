package com.example.minhastarefas

class TarefaController {
    private var lista: ArrayList<Tarefa>
    private var dao: TarefaDAO

    constructor(dao: TarefaDAO) {
        this.dao = dao
        this.lista = dao.get()
    }

    fun add(tarefa: Tarefa) {
        this.dao.inserir(tarefa)
        this.lista.add(tarefa)
        this.lista = dao.get()
    }

    fun get(index: Int) : Tarefa {
        return this.lista.get(index)
    }

    fun get(): ArrayList<Tarefa> {
        return this.lista
    }

    fun count(): Int {
        return this.lista.count()
    }

    fun delete(tarefa: Tarefa) {
        this.dao.delete(tarefa)
        this.lista = dao.get()
    }

    fun update(tarefa: Tarefa) {
        this.dao.update(tarefa)
        this.lista = dao.get()
    }

}