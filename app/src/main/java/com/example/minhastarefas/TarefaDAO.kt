package com.example.minhastarefas

import android.content.ContentValues
import android.content.Context
import java.io.Serializable

class TarefaDAO: Serializable {
    var banco: BancoHelper

    constructor(context: Context) {
        this.banco = BancoHelper(context)
    }

    fun inserir(tarefa: Tarefa) {
        val cv = ContentValues()
        cv.put("descricao", tarefa.descricao)
        cv.put("prioridade", tarefa.prioridade)
        cv.put("status", tarefa.status)
        this.banco.writableDatabase.insert("tarefas",null, cv)
    }

    fun count(): Int {
        val colunas = arrayOf("id")
        val c = this.banco.readableDatabase.query("tarefas", colunas, null, null, null, null, null)
        return c.count
    }

    fun get(): ArrayList<Tarefa> {
        val lista = ArrayList<Tarefa>()
        val colunas = arrayOf("id", "descricao", "prioridade", "status")
        val c = this.banco.readableDatabase.query("tarefas", colunas, null, null, null, null, "status, prioridade DESC")
        if (c.count > 0) {
            c.moveToFirst()
            do {
                val id = c.getInt(c.getColumnIndex("id"))
                val descricao = c.getString(c.getColumnIndex("descricao"))
                val prioridade = c.getInt(c.getColumnIndex("prioridade"))
                var status = c.getInt(c.getColumnIndex("status"))
                var ativo = if (status == 1) true else false
                lista.add(Tarefa(id, descricao, ativo, prioridade))
            } while (c.moveToNext())
        }
        return lista
    }

    fun get(id: Int): Tarefa? {
        val lista = ArrayList<Tarefa>()
        val colunas = arrayOf("id", "descricao", "prioridade", "status")
        val c = this.banco.readableDatabase.query("tarefas", colunas, null, null, null, null, null)
        if (c.count > 0) {
            c.moveToFirst()
            do {
                val id = c.getInt(c.getColumnIndex("id"))
                val descricao = c.getString(c.getColumnIndex("descricao"))
                val prioridade = c.getInt(c.getColumnIndex("prioridade"))
                val status = c.getInt(c.getColumnIndex("status"))
                var ativo = if (status == 1) true else false
                lista.add(Tarefa(id, descricao, ativo, prioridade))
            } while (c.moveToNext())
        }

        for (tarefa in lista) {
            if (tarefa.id == id) return tarefa
        }

        return null
    }

    fun delete(tarefa: Tarefa) {
        val where = "id = ?"
        val wherep = arrayOf(tarefa.id.toString())
        this.banco.writableDatabase.delete("tarefas", where, wherep)
    }

    fun update(tarefa: Tarefa) {
        val cv = ContentValues()
        val where = "id = ?"
        val wherep = arrayOf(tarefa.id.toString())
        cv.put("descricao", tarefa.descricao)
        cv.put("prioridade", tarefa.prioridade)
        cv.put("status", tarefa.status)
        this.banco.writableDatabase.update("tarefas", cv, where, wherep)
    }
}