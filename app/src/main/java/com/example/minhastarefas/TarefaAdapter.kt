package com.example.minhastarefas

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView

class TarefaAdapter (var controller: TarefaController, var context: Context): BaseAdapter() {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val tarefa = this.controller.get(position)
        val linha : View

        if (convertView == null) {
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            linha = inflater.inflate(R.layout.tarefa, null)
        } else {
            linha = convertView
        }

        val tvDescricao = linha.findViewById<TextView>(R.id.tvTarefaDescricao)
        val tvPrioridade = linha.findViewById<TextView>(R.id.tvTarefaPrioridade)
        val ivIcone = linha.findViewById<ImageView>(R.id.ivTarefaIcone)

        tvDescricao.text = tarefa.descricao
        tvPrioridade.text = "Prioridade: ${tarefa.prioridade.toString()}"
        var color = if (tarefa.status) "#00aa00" else "#aa0000"
        ivIcone.setColorFilter(Color.parseColor(color))
        var imageIcon: Drawable

        if (tarefa.status) {
            imageIcon = this.context.resources.getDrawable(R.drawable.ic_check_box_black_24dp)
        } else {
            imageIcon = this.context.resources.getDrawable(R.drawable.ic_check_box_outline_blank_black_24dp)
        }

        ivIcone.setImageDrawable(imageIcon)

        return linha
    }

    override fun getItem(position: Int): Any {
        return this.controller.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return this.controller.count()
    }
}