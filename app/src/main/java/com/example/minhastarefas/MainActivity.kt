package com.example.minhastarefas

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.BaseAdapter
import android.widget.ListView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MainActivity : AppCompatActivity() {
    private lateinit var lvTarefas: ListView
    private lateinit var tarefaController: TarefaController
    private lateinit var fabAddTarefa: FloatingActionButton
    private lateinit var listaTarefas: ArrayList<Tarefa>

    private val TAREFA = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // CONTROLLER DE TAREFA
        this.tarefaController = TarefaController(TarefaDAO(this))

        this.listaTarefas = tarefaController.get()

        // PEGA BOTAO ADD E LISTVIEW
        this.fabAddTarefa = findViewById(R.id.fabMainAddTarefa)
        this.lvTarefas = findViewById(R.id.lvMainTarefas)

        // ADD LISTENER AO FAB
        this.fabAddTarefa.setOnClickListener(OnClickFAB())

        this.lvTarefas.adapter = TarefaAdapter(this.tarefaController, this)

        this.lvTarefas.setOnItemClickListener( ClickList() )
        this.lvTarefas.setOnItemLongClickListener( LongClickList() )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK && requestCode == TAREFA) {

            if (data?.hasExtra("ADD") == true) {
                var tarefaAdd = data?.getSerializableExtra("ADD") as Tarefa
                this.tarefaController.add(tarefaAdd)
            } else if (data?.hasExtra("UP") == true) {
                var tarefaUp = data?.getSerializableExtra("UP") as Tarefa
                this.tarefaController.update(tarefaUp)
            }
            this.updateTaskList()
        }
    }

    fun updateTaskList() {
        this.listaTarefas = this.tarefaController.get()
        (this.lvTarefas.adapter as BaseAdapter).notifyDataSetChanged()
    }

    inner class OnClickFAB: View.OnClickListener {
        override fun onClick(v: View?) {
            val it = Intent(this@MainActivity, AddTarefaActivity::class.java)
            startActivityForResult(it, TAREFA)
        }
    }

    inner class ClickList: AdapterView.OnItemClickListener {
        override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            val intent = Intent(this@MainActivity, AddTarefaActivity::class.java)
            val tarefa = this@MainActivity.lvTarefas.adapter.getItem(position) as Tarefa
            intent.putExtra("TAREFA", tarefa)
            startActivityForResult(intent, TAREFA)
        }
    }

    inner class LongClickList: AdapterView.OnItemLongClickListener {
        override fun onItemLongClick(
            parent: AdapterView<*>?,
            view: View?,
            position: Int,
            id: Long
        ): Boolean {
            val tarefa = this@MainActivity.lvTarefas.adapter.getItem(position) as Tarefa

            val adDel = AlertDialog.Builder(this@MainActivity)
            adDel.setTitle("Excluir")
            adDel.setIcon(R.mipmap.ic_launcher)
            adDel.setMessage("Você realmente quer excluir?")

            adDel.setPositiveButton("Excluir") {dialog, which ->
                this@MainActivity.tarefaController.delete(tarefa)
                this@MainActivity.updateTaskList()
            }

            adDel.setNegativeButton("Cancelar") {dialog, which ->
                Toast.makeText(this@MainActivity, "Cancelado", Toast.LENGTH_SHORT).show()
            }

            adDel.create().show()

            return true
        }
    }


}
