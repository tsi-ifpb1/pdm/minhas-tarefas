package com.example.minhastarefas

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*

class AddTarefaActivity : AppCompatActivity() {
    private lateinit var etDescricao: EditText
    private lateinit var sbPrioridade: SeekBar
    private lateinit var swStatus: Switch
    private lateinit var tvStatus: TextView
    private lateinit var btCancelar: Button
    private lateinit var btSalvar: Button
    private lateinit var tarefaController: TarefaController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_tarefa)

        this.tarefaController = TarefaController(TarefaDAO(this))

        this.etDescricao = findViewById(R.id.etAddTarefaDescricao)
        this.tvStatus = findViewById(R.id.tvAddTarefaStatus)
        this.swStatus = findViewById(R.id.swAddTarefaStatus)
        this.sbPrioridade = findViewById(R.id.sbAddTarefaPrioridade)
        this.btCancelar = findViewById(R.id.btAddTarefaCancelar)
        this.btSalvar = findViewById(R.id.btAddTarefaSalvar)

        this.btSalvar.setOnClickListener { salvar(it)}
        this.btCancelar.setOnClickListener { cancelar(it) }
        this.swStatus.setOnCheckedChangeListener( Mudou())

        if (intent.hasExtra("TAREFA")) {
            val tarefa = intent.getSerializableExtra("TAREFA") as Tarefa
            this.etDescricao.setText(tarefa.descricao)
            this.sbPrioridade.progress = tarefa.prioridade
            this.swStatus.isChecked = tarefa.status
            val color = if (tarefa.status) "#00aa00" else "#aa0000"
            this.tvStatus.setBackgroundColor(Color.parseColor(color))
        }
    }

    private fun salvar(view: View) {
        val descricao = this.etDescricao.text.toString()
        val prioridade = this.sbPrioridade.progress
        val status = this.swStatus.isChecked

        var tarefa: Tarefa
        var operacaoDAO = "ADD"

        if (intent.hasExtra("TAREFA")) {
            var tarefaIntent = intent.getSerializableExtra("TAREFA") as Tarefa
            tarefa = Tarefa(tarefaIntent.id, descricao, status, prioridade)
            operacaoDAO = "UP"
        } else {
            tarefa = Tarefa(descricao, status, prioridade)
        }

        val intent = Intent()
        intent.putExtra(operacaoDAO, tarefa)

        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    private fun cancelar(view: View) {
        finish()
    }

    inner class Mudou: CompoundButton.OnCheckedChangeListener {
        override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
            val color = if(isChecked) "#00aa00" else "#aa0000"
            this@AddTarefaActivity.tvStatus.setBackgroundColor(Color.parseColor(color))
        }

    }
}
