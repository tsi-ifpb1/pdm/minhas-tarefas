package com.example.minhastarefas

import java.io.Serializable

class Tarefa: Serializable {
    var id: Int
    var descricao: String
    var prioridade: Int
    var status: Boolean

    constructor(descricao: String, status: Boolean, prioridade: Int) {
        this.id = -1
        this.descricao = descricao
        this.prioridade = prioridade
        this.status = status
    }

    constructor(id: Int, descricao: String, status: Boolean, prioridade: Int) {
        this.id = id
        this.descricao = descricao
        this.prioridade = prioridade
        this.status = status
    }

    override fun toString(): String {
        return "${id} - ${descricao} - ${prioridade} - ${status}"
    }
}